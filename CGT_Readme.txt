1. Setting up and building Yocto
--------------------------------

Installation in Ubuntu 12.04.3 64 bit [Clean install]

Some packages are needed:
 
	$ sudo apt-get update
	$ sudo apt-get install gawk wget git-core diffstat unzip texinfo build-essential chrpath \
	  libsdl1.2-dev xterm make xsltproc docbook-utils fop dblatex xmlto autoconf automake libtool \
	  libglib2.0-dev curl cifs-utils nfs-kernel-server vim uboot-mkimage
	  
To get the yocto recipes:

   $ mkdir ~/yocto
   $ cd ~/yocto
   $ git clone https://git.congatec.com/yocto-1-5/bsp-scripts.git .
   $ ./cgt-yocto_fetch.sh
   
To build the Graphical-User-Interface image for Congtec QMX6:

   $ MACHINE=cgtqmx6 source setup-environment build_cgtqmx6
	  [ ! ] EULA accept needed for next step.

   The part number of the target QMX6 must be specified in the conf/local.conf file following the next steps:
   
   $ gedit conf/local.conf
	 
	 [!]add at the end of the file:
	 
	   PARTNUMBER = "PN016xxxx" 
	   
	 [!]Save, and exit.  

   $ bitbake fsl-image-gui
   
The process will take hours.

When it finishes the image will be located ~/yocto/build_cgtqmx6/tmp/deploy/images/cgtqmx6/

2. Transfer the root file system
---------------------------------

2.1. micro SD
--------------
In order to transfer the image to a uSD card, follow the next steps changing sdX for your detected device:

	$ cd ~/yocto/build_cgtqmx6/tmp/deploy/images/cgtqmx6/
	$ sudo dd if=/dev/zero of=/dev/sdX count=1000 bs=512
	$ sudo sfdisk --force -uM /dev/sdX <<EOF
		10,,83
		EOF
	$ sudo mkfs.ext3 -j /dev/sdX1
	$ sudo mount /dev/sdX1 /mnt
	$ sudo tar -xjvf fsl-image-gui-cgtqmx6-xxxxxxxxxxxxxx.tar.bz2 -C /mnt
	$ sync
	$ sudo umount /dev/sdX1

2.2. eMMC
----------
An uSD is needed as a bridge, then the first step is follow the steps of "Transfer the root file system to a micro SD card", and transfer the image tar.bz2 file to the micro SD with the following command:

	$ cd ~/yocto/build_cgtqmx6/tmp/deploy/images/cgtqmx6/
	$ sudo mount /dev/sdX1 /mnt
	$ sudo cp fsl-image-gui-cgtqmx6-xxxxxxxxxxxxxx.tar.bz2 /mnt
	$ sudo cp [BSP-FOLDER]/tools/fdisk.input
	$ sync
	$ sudo umount /dev/sdX1
	
Boot the system and:
	
	$ sudo dd if=/dev/zero of=/dev/mmcblk1 count=1000 bs=512
	$ fdisk /dev/mmcblk1 < fdisk.input
	$ sudo mkfs.ext3 -j /dev/mmcblk1p1
	$ sudo mount /dev/mmcblk1p1 /mnt
	$ sudo tar -xjvf fsl-image-gui-cgtqmx6-xxxxxxxxxxxxxx.tar.bz2 -C /mnt
	$ sync
	
Shut down the system and remove the microSD card.

	
	