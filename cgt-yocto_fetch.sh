#!/bin/bash

mkdir -p sources
cd sources

git clone https://git.congatec.com/yocto-1-5/meta-fsl-arm-extra.git
git clone https://git.congatec.com/yocto-1-5/meta-fsl-arm.git
git clone https://git.congatec.com/yocto-1-5/base.git
git clone https://git.congatec.com/yocto-1-5/meta-fsl-demos.git
git clone https://git.congatec.com/yocto-1-5/meta-openembedded.git
git clone https://git.congatec.com/yocto-1-5/poky.git

cd meta-fsl-arm-extra
git checkout yocto-qmx6-4.1.0

echo " ---------
   DONE
----------"

